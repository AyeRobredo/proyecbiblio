class SessionController < ApplicationController
	layout 'application'
	def new
 #necesito el formulario para la creación de la sesion
	end

	def create

	  user = User.find_by_email(params[:session][:email])
	  if user && user.authenticate(params[:session][:password])
	    session[:user_id] = user.id
	    redirect_to books_path
	  else
			flash[:alert] = "Email o Contraseña incorrectos"
	    redirect_to welcome_index_path

	  end
	end

	def destroy
	  session[:user_id] = nil
	  redirect_to welcome_index_path
	end
end
