class BooksController < ApplicationController


	before_action :find_book, only: [:show,:edit,:update,:destroy]

  load_and_authorize_resource

  def index
    @books = Book.all
		if params[:search]
      @books = Book.search(params[:search]).order("created_at DESC")
    else
      @books = Book.all.order('created_at DESC')
		end
  end




  def show

  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new(book_params)
    if @book.save
      redirect_to @book
    else
      render :new
    end
  end

  def book_params
      params.require(:book).permit(:isbn, :name, :author, :tag, :published_at, :state)
  end

  def edit
  end

  def update
    if @book.update(book_params)
      redirect_to @book
    else
      render :edit
    end
  end

  def destroy
    @book.destroy
    redirect_to books_path
  end

  def find_book
    @book = Book.find(params[:id])
  end


end
