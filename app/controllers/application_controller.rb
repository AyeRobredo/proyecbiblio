class ApplicationController < ActionController::Base
	helper_method :current_user
	#before_action :require_operator


  def current_user
    @current_user ||= User.find_by_id(session[:user_id]) if session[:user_id]
  end

  def require_user
    redirect_to login_path unless current_user
  end

	rescue_from CanCan::AccessDenied do |exception|
   redirect_to root_path, :alert => exception.message
 end


  # def require_operator
   # redirect_to login_path unless current_user.role == 'operator'
  #end

end
