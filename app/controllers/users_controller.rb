class UsersController < ApplicationController

	before_action :find_user, only: [:show,:edit,:update,:destroy]

  load_and_authorize_resource

 	def index
    @users = User.all
    if params[:search]
      @users = User.search(params[:search]).order("created_at DESC")
    else
      @users = User.all.order('created_at DESC')
    end
	end

  def show
  end

  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to @user
    else
      render :new
    end
  end

  def user_params
      params.require(:user).permit(:first_name, :last_name, :dni, :email, :telephone, :role, :password, :password_confirmation)
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to @user
    else
      render :edit
    end
  end

  def destroy
    @user.destroy
    redirect_to users_path
  end

  def find_user
    @user = User.find(params[:id])
  end


end
