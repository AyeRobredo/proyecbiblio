class LoansController < ApplicationController
	before_action :find_loan, only: [:show,:edit,:update,:destroy]

  load_and_authorize_resource

	def index
    @loans = Loan.all
		@loans = @loans.map { |loan| loan.is_expired? }
    if params[:search]
      @loans = Loan.search(params[:search]).order("created_at DESC")
    else
      @loans = Loan.all.order('created_at DESC')
    end
	end

  def show
  end

  def new
    @loan = Loan.new
  end

  def create
    @book = Book.find_by_id(params[:loan][:book_id])

    if @book.available?
      puts loan_params

      @loan = Loan.new(loan_params)
      @loan.save
      redirect_to @loan
    else
			 flash[:alert] ="Libro no disponible"
			 redirect_to loans_path

    end
  end


  #def create
   # @loan = Loan.new(loan_params)
    #if @loan.save
     # redirect_to @loan
    #else
     # render :new
    #end
  #end

  def loan_params
      params.require(:loan).permit(:user_id, :book_id, :initiation_date, :final_date, :state)
  end

  def edit
  end

  def update
    if @loan.update(loan_params)
      redirect_to @loan
    else
      render :edit
    end
  end

  def destroy
    @loan.destroy
    redirect_to loans_path
  end

  def find_loan
    @loan = Loan.find(params[:id])
  end


end
