class Book < ApplicationRecord
	has_many :loans
	has_many :users, through: :loans

	before_validation :set_state, on: :create

	def available?
		loans.where(state: "actived").or(loans.where(state: "expired")).count == 0
	end

	def available
		if self.available?
			self.state = "available"
			dispo = {"available" => "Disponible"}
			dispo[self.state]
		else
			self.state = "not available"
			no_dispo = {"not available" => "No Disponible"}
			no_dispo[self.state]
		end
	end

	def self.search(search)
  		authors = where("author LIKE ?", "%#{search}%")
  		names = where("name LIKE ?", "%#{search}%")
		Book.from("(#{authors.to_sql} UNION #{names.to_sql}) AS books")
	end

	def set_state
			self.state = "available"
	end


end
