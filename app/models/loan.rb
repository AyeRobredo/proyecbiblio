class Loan < ApplicationRecord
	belongs_to :user
	belongs_to :book

	before_validation :set_state, on: :create


	def self.search(search)
		users_id = where("user_id LIKE ?", "%#{search}%")
		states = where("state LIKE ?", "%#{search}%")
	Loan.from("(#{users_id.to_sql} UNION #{states.to_sql}) AS loans")
	end

	def set_state
	    self.state = "actived"
	end

	def estado
		estados = {"actived" => "Activo", "expired" => "Expirado", "finished" => "Finalizado"}
		estados[self.state]
	end

	#PARA PONER EL ESTADO DEL PRESTAMO EN VENCIDO (EXPIRED)
		def is_expired?
			if self.final_date < DateTime.now && self.state == "actived"
				 self.state = "expired"
				 self.save
			end
			self.state == "expired"
		end

		#LISTAR LOS PRESTAMOS VENCIDOS.
			def self.expired
				Loan.where(state: "expired")
			end

end
