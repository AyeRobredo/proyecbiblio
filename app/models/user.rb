class User < ApplicationRecord

	has_secure_password
	has_many :loans
	has_many :books, through: :loans



	def self.search(search)
      last_names = where("last_name LIKE ?", "%#{search}%")
      dnis = where("dni LIKE ?", "%#{search}%")
    Book.from("(#{last_names.to_sql} UNION #{dnis.to_sql}) AS books")
	end


	def student?
    	self.role == 'Alumno'
  	end

  	def librarian?
   		self.role == 'Bibliotecario'
 	end

 	def preceptor?
   		self.role == 'Preceptor'
 	end

 	def professor?
   		self.role == 'Docente'
 	end


end
