class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.integer :dni
      t.string :email, index: true, unique: true, null: false
      t.string :telephone
      t.string :role, default: 'user'
      t.string :password

      t.timestamps
    end
  end
end
