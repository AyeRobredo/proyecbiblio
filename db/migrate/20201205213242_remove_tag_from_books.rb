class RemoveTagFromBooks < ActiveRecord::Migration[6.0]
  def change
    remove_column :books, :tag, :string
  end
end
