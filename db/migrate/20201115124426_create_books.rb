class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :isbn
      t.string :name
      t.string :author
      t.string :type
      t.string :tag
      t.datetime :published_at
      t.string :state

      t.timestamps
    end
  end
end
